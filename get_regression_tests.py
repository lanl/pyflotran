import pickle
import os

testlog = "pflotran-tests-2020-08-10_07-12-25.testlog"  # change me

# -------------------------- #

file = os.path.join(os.environ["PFLOTRAN_DIR"], "regression_tests", testlog)
write_file = "regression_input_filenames.pickle"

count = 0
tests = []
print("--> Reading")
with open(file, "r") as f:
	lines = f.readlines()
	for line in lines:
		#        print line
		if "... " in line:
			if len(line.split()) == 1:
				tests.append(line.replace(".", " ").split()[0] + ".in")
				count += 1

print("--> Tests are:")
print(tests)
print("--> Total tests: " + str(count))
print("--> Writing test input files")

with open(write_file, "wb") as f:
	pickle.dump(tests, f)
