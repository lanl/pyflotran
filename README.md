## About PyFLOTRAN ##
PyFLOTRAN is a set of classes and methods that the enable use of the massively parallel subsurface flow and reactive transport code PFLOTRAN (http://www.pflotran.org) within the Python scripting environment.
This allows the use of a wide range of libraries available in Python for pre- and post- processing. The main advantages of using PyFLOTRAN include:

* PFLOTRAN input file construction from Python environment. Reduces the need to learn all the keywords in PFLOTRAN.
* Post-processing of output using matplotlib as well as including Python-based commandline calls to the open-source visualization toolkits such as VisIt and Paraview.
Scripting tools that supports Python's built-in multi-processing capabilities for batch simulations.
This reduces the time spent in creating separate input files either for multiple realizations or multiple simulations for comparison.
* Streamlined workflow from pre-processing to post-processing. Typical workflow with PFLOTRAN involves -- pre-processing in Python or MATLAB, writing input files, executing the input files and then post-processing using matplotlib, VisIt or Paraview.
* PyFLOTRAN allows users to perform all these steps using one Python script.

## Installation ##

To install PyFLOTRAN using ``pip``:

``pip install https://gitlab.com/lanl/pyflotran.git``

To install PyFLOTRAN using ``git``:

``git clone https://gitlab.com/lanl/pyflotran.git``

To remove PyFLOTRAN, type:

``pip uninstall pyflotran``

See https://bitbucket.org/pyflotran/pyflotran/wiki/Home for more details.

## Testing

Testing in PyFLOTRAN is strongly informed by the PFLOTRAN regression testing suite.
Consequently, the PFLOTRAN regression tests must be performed before any PyFLOTRAN testing can begin.

### 1. Running the PFLOTRAN regression tests

[To run the regression suite](https://www.pflotran.org/documentation/user_guide/how_to/regression.html?highlight=test#regression-test-manager),

```bash
$ cd $PFLOTRAN_DIR/regression_tests
$ make test
```

(Note: the command `make clean-tests` removes testing artifacts that PyFLOTRAN needs. Don't run it!)

### 2. Preparing the PyFLOTRAN test suite

After `make test` completes, it will write out its results to a file with a name similar to `pflotran-tests-2020-08-10_07-12-25.testlog`.

Open the file `get_regression_tests.py` (in the directory where PyFLOTRAN is) and change the variable `testlog` to the name of the PFLOTRAN testlog file (i.e. `pflotran-tests-2020-08-10_07-12-25.testlog`).

Then, run:

```bash
python get_regression_tests.py
```

### 3. Running PyFLOTRAN tests

The test suite is `regression.py`. Typically, one will run this as:

```bash
python regression.py --all
```

The full capabilities of the PyFLOTRAN test suite are below:

```
usage: regression.py [-h] [-a] [-r] [-v] [-s SINGLE] [-d DIFF] [-rw READWRITE]

Performs a two-proged analysis on PyFLOTRAN files. In the 'validation' approach, multiple PFLOTRAN input files are read into PyFLOTRAN and written out.
This should be considered a 'first-pass' for verifying I/O, functional, and class capabilities of PyFLOTRAN.
The second and more rigorous test is to perform a regression analysis on output from PFLOTRAN: this is done by a comparitive analysis on PFLOTRAN output from a 'gold standard' input file and from a PyFLOTRAN generated input file.

optional arguments:
  -h, --help            show this help message and exit
  -a, --all             Run both validation and regression tests.
  -r, --regression      Run *only* the comparative regression analysis.
  -v, --validation      Run *only* the PyFLOTRAN I/O validation.
  -s SINGLE, --single SINGLE
                        Run a comparative regression analysis on a single
                        PFLOTRAN input file.
  -d DIFF, --diff DIFF  Compare read and written PFLOTRAN input files for
                        missing keywords.
  -rw READWRITE, --readwrite READWRITE
```